#ifndef PATH_H
#define PATH_H

class Path {
    enum Turn {
      INVALID = -1,
      LEFT,
      RIGHT,
      STRAIGHT
    };
  private:
    bool b_validPath = false;
    int numCommands = 0;
    int commandList_size = 10;
    // put this into PROGMEM if there are memory issues
    Turn commandList[];

  public:
    Path() { }

    void reset() {
      this->numCommands = 0;
      this->b_validPath = true;
      this->commandList_size = 10;
      //this->commandList = Turn[this->commandList_size];
    }
    bool addSegment(const int &command) {
      int newSize = this->commandList_size + 1;
      if (this->commandList_size < newSize) {
        Turn newList[newSize * 2];
        memcpy(newList, this->commandList, sizeof(Turn) * newSize);
      }
      Turn newCommand;
      switch (command) {
        case 0:
          newCommand = Turn::LEFT;
          break;
        case 1:
          newCommand = Turn::RIGHT;
          break;
        case 2:
          newCommand = Turn::STRAIGHT;
          break;
        default:
          b_validPath = false;
          newCommand = Turn::INVALID;
          break;
      }
      this->commandList[numCommands++] = newCommand;
    }

    bool isValid() {
      return this->b_validPath;
    }

    int getNumCommands() {
      return this->numCommands;
    }

    Turn getTurn(const int &turnNum) {
      if (turnNum > this->numCommands) {
        return Turn::INVALID;
      }
      return this->commandList[turnNum];
    }
};

#endif

