const int pin = A2;
const int numReadings = 100;

void setup() {
  Serial.begin(9600);
  pinMode(pin, INPUT);
}

void loop() {
  double sum_v = 0;
  for (int i = 0; i < numReadings; ++i) {
    sum_v+= (analogRead(pin) * 5.0 / 1024);
    delay(30);
  }
  Serial.println(sum_v / numReadings);
}
