#ifndef IR_READER_H
#define IR_READER_H

#include "Kalman_1D.h"

class IR_Reader {
  public:
    enum IR_TYPE {
      OLD, NEW
    };
    
    IR_Reader(const int &input_pin, IR_TYPE type) {
      pinMode(input_pin, INPUT);
      this->pin = input_pin;
      this->ir_type = type;
    }

    // Distances all in cm
    double getReading() {
      int analogVal = analogRead(this->pin);
      double dist_cm;
      switch (this->ir_type) {
        case IR_TYPE::OLD:
          dist_cm = avg_old();
          //dist_cm  = 13.433 * pow(analogVal * 5.0 / 1023, -1.464);
          //return this->kalman_filter.update_step(constrain(dist_cm, 2.5, 37.5));
          return constrain(dist_cm, 2.5, 37.5);
          break;
        case IR_TYPE::NEW:
          dist_cm = 12.075 * pow(analogVal*5.0/1024.0, -1.086);
          //return constrain(dist_cm, 2.5, 37.5);
          return this->kalman_filter.update_step(constrain(dist_cm, 2.5, 37.5));
          break;
      }
      //return constrain(dist_cm, 2.5, 37.5);
      //return this->kalman_filter.update_step(constrain(dist_cm, 2.5, 37.5));
      return -1;
    }

  private:
    Kalman_1D kalman_filter;
    int pin;
    IR_TYPE ir_type;

    double avg_old() {
      double num = 5;
      double sum = 0;
      double val;
      for (int i = 0; i < num; ++i) {
        val = analogRead(this->pin);
        sum+=13.433 * pow(val * 5.0 / 1023, -1.464);
        delay(25);
      }
      return sum / num;
    }
};

#endif

