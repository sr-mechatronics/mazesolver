#include "MazeSegmenter.h"

#include "opencv2/imgproc/imgproc.hpp"

MazeSegmenter::MazeSegmenter() {}

// Segments and creates a mask based on passed values.
void MazeSegmenter::segment(const cv::Mat &src, cv::Mat &dst, const double &h, const double &h_err, const double &s, const double &s_err, const double &v, const double &v_err) {
	cv::Scalar low(h - h_err / 2, s - s_err / 2, v - v_err / 2);
	cv::Scalar high(h + h_err / 2, s + s_err / 2, v + v_err / 2);
	cv::inRange(src, low, high, dst);
}

// Tries to find the largest rectangle and then rectifies the image using Homographies, and extracts the ROI
bool MazeSegmenter::rectify(const cv::Mat &src, const cv::Mat &src_corners, cv::Mat &dst, const int &epsilon) {
	cv::Mat blurred;
	cv::GaussianBlur(src_corners, blurred, cv::Size(5, 5), 0, 0);

	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;

	std::vector<std::vector<cv::Point>> polyList;

	cv::findContours(blurred, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
	for (int i = 0; i < contours.size(); ++i) {
		std::vector<cv::Point> poly;
		cv::approxPolyDP(contours[i], poly, epsilon, true);

		// thresh the poly on number of sides
		if (poly.size() != 4) { continue; }

		// thresh the poly on length and width
		cv::Rect rect = cv::boundingRect(poly);
		double height_ratio = ((double)rect.height) / src.rows;
		double width_ratio = ((double)rect.width) / src.cols;
		if ((height_ratio < .05) || (height_ratio > .95) || (width_ratio < .05) || (width_ratio > .95)) { continue; }

		reorderQuadPoints(poly);
		polyList.push_back(poly);
	}


	cv::Point2f targetCorners[4], srcCorners[4];
	std::vector<cv::Point> poly(4);
	poly[0] = cv::Point(0, 0);
	poly[1] = cv::Point(0, src.rows - 1);
	poly[2] = cv::Point(src.cols - 1, src.rows - 1);
	poly[3] = cv::Point(src.cols - 1, 0);
	reorderQuadPoints(poly);

	targetCorners[0] = poly[0];
	targetCorners[1] = poly[1];
	targetCorners[2] = poly[2];
	targetCorners[3] = poly[3];

	if (polyList.size() == 1) {
		poly = polyList[0];
	} else if (polyList.size() > 1) {
		poly = polyList[0];
		// get the contour with the largest area
		for (int i = 1; i < polyList.size(); ++i) {
			double a_curBest_poly = cv::contourArea(poly);
			double a_cur_poly = cv::contourArea(polyList[i]);
			if (a_curBest_poly < a_cur_poly) {
				poly = polyList[i];
			}
		}
	} else {
		return false;
	}
	srcCorners[0] = poly[0];
	srcCorners[1] = poly[1];
	srcCorners[2] = poly[2];
	srcCorners[3] = poly[3];
	// rectify the image by getting the affine warp from srcCorners to targetCorners (rectangle)
	//img_rectified = cv::Mat::zeros(hsv.rows, hsv.cols, hsv.type());
	cv::Mat tform = cv::getPerspectiveTransform(srcCorners, targetCorners);
	cv::warpPerspective(src, dst, tform, src.size());

	return true;
}

// Attempt at a fast skeleton. For use with the maze itself.
void MazeSegmenter::makeSkeleton(const cv::Mat &src, cv::Mat &dst) {
	cv::Mat ret;
	src.copyTo(ret);
	cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(6, 6));
	for (int i = 0; i < 1; ++i) {
		cv::dilate(ret, ret, element);
	}
	for (int i = 0; i < 7; ++i) {
		cv::erode(ret, ret, element);
	}
	dst = ret;
}

// gets the center of the largest blob of an image. Segment/Mask on colors first.
bool MazeSegmenter::extractBlobs(const cv::Mat &src, cv::Point &result) {
	cv::Mat working;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	int prev_bestArea = -1;
	cv::Rect bestRect(-1, -1, 1, 1);

	working = src.clone();
	cv::GaussianBlur(working, working, cv::Size(5, 5), 0, 0);
	cv::findContours(working, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
	for (int i = 0; i < contours.size(); ++i) {
		//cv::drawContours(img_contours, contours, i, cv::Scalar(0, 255, 0, 0), 2, 7, hierarchy, 0, cv::Point());
		std::vector<cv::Point> poly(4);
		cv::approxPolyDP(contours[i], poly, 10, true);

		cv::Rect rect = cv::boundingRect(poly);

		if (prev_bestArea < (rect.width*rect.height)) {
			prev_bestArea = (rect.width*rect.height);
			bestRect = rect;
		}
	}
	result = cv::Point(bestRect.x + bestRect.width / 2, bestRect.y + bestRect.height / 2);
	return prev_bestArea > -1;
}

// extracts a path between two points given a skeleton. 
bool MazeSegmenter::getPath(cv::Mat &costMap, const cv::Point &end, std::vector<cv::Point> &pathPoints) {
	if (!traverseCostMap(costMap, end, pathPoints)) { return false; }
	simplifyPoints2Line(pathPoints, pathPoints, threshold_points2Lines);
	simplifyPointsByDirection(pathPoints, pathPoints);
	return true;
}

// extracts a path between two points given a skeleton. 
void MazeSegmenter::getCostMap(const cv::Mat &src, const cv::Point &start, cv::Mat &costMap) {
	bfs_path(src, start, costMap);
	return;
}

// simplifies branches
void simplifyAllPaths(std::vector<std::vector<cv::Point>> &allPaths) {
	std::vector<cv::Point> fromStart;
	for (std::vector<cv::Point> path : allPaths) {

	}
}

// Gets the branches and paths from start to end of all paths
void MazeSegmenter::getAllBranches(const cv::Mat &costMap, const cv::Point &start, std::vector<cv::Point> &points) {
	std::vector<cv::Point> ends;
	cv::Point lMaxima;
	
	const int rows = costMap.rows, cols = costMap.cols;

	// dividing factor is number of "cells" in a dimension
	const int WINDOW_V = rows / 8, WINDOW_H = cols / 8;  

	int curRow_start = 1, curRow_end, curCol_start = 1, curCol_end;
	bool b_colEnd = false;
	while (curRow_start < rows) {
		b_colEnd = (curCol_start + WINDOW_H) > (cols - 1);
		curCol_end = b_colEnd ? cols -1 : curCol_start + WINDOW_H;

		curRow_end = std::min(curRow_start + WINDOW_V, rows - 1);

		if (getLocalMaxima(costMap, curRow_start, curRow_end, curCol_start, curCol_end, lMaxima)) {
			ends.push_back(lMaxima);
		}

		if (b_colEnd) {
			curCol_start = 0;
			curRow_start += WINDOW_V / 2;
		} else {
			curCol_start += WINDOW_H / 2;
		}
	}
	points = ends;
}
// Gets the local maxima of a region. Mat is single channle mat of ints, and 0 is ignored.
bool MazeSegmenter::getLocalMaxima(const cv::Mat &src, const int &y, const int &y_end, const int &x, const int &x_end, cv::Point &maxima) {
	maxima = cv::Point(-1, -1);
	int curMax = 0; // 0 is invalid, and all greater than are valid.
	int curVal;

	for (int j = y; j < y_end; ++j) {
		const int* srcRow = src.ptr<int>(j);
		for (int i = x; i < x_end; ++i) {
			curVal = srcRow[i];
			if (curVal > curMax) {
				curMax = curVal;
				maxima = cv::Point(i, j);
			}
		}
	}
	if (y < maxima.y && maxima.y < y_end && x < maxima.x && maxima.x < x_end) {
		if ((src.at<int>(cv::Point(maxima.x - 1, maxima.y)) > 0)
			&& (src.at<int>(maxima) -src.at<int>(cv::Point(maxima.x - 1, maxima.y)) < 0)) {
			return false;
		}
		if ((src.at<int>(cv::Point(maxima.x + 1, maxima.y)) > 0)
			&& src.at<int>(maxima) -src.at<int>(cv::Point(maxima.x + 1, maxima.y)) < 0) {
			return false;
		}
		if ((src.at<int>(cv::Point(maxima.x, maxima.y - 1)) > 0)
			&& src.at<int>(maxima) -src.at<int>(cv::Point(maxima.x, maxima.y - 1)) < 0) {
			return false;
		}
		if ((src.at<int>(cv::Point(maxima.x, maxima.y + 1)) > 0)
			&& src.at<int>(maxima) -src.at<int>(cv::Point(maxima.x, maxima.y + 1)) < 0) {
			return false;
		}
		return true;
	} else { return false; }
}

#pragma region SEARCH
// enqueues and marks adjacent valid nodes (points)
void MazeSegmenter::enqueueMarkAdj(cv::Point &pt, std::queue<cv::Point> &queue, cv::Mat &exploration, cv::Mat &costMap) {
	// left right up down
	int maxCol = exploration.cols, maxRow = exploration.rows;
	int curDist = costMap.at<int>(pt);

	cv::Point left(pt.x - 1, pt.y);
	cv::Point right(pt.x + 1, pt.y);
	cv::Point up(pt.x, pt.y - 1);
	cv::Point down(pt.x, pt.y + 1);

	bool validLeft = (0 < left.x) && (left.x < maxCol - 1) && (0 < left.y) && (left.y < maxRow - 1);
	bool validRight = (0 < right.x) && (right.x < maxCol - 1) && (0 < right.y) && (right.y < maxRow - 1);
	bool validUp = (0 < up.x) && (up.x < maxCol - 1) && (0 < up.y) && (up.y < maxRow - 1);
	bool validDown = (0 < down.x) && (down.x < maxCol - 1) && (0 < down.y) && (down.y < maxRow - 1);

	if (right.x == 154 && right.y == 190) {
		int rightCost = costMap.at<int>(right);
		right.x = right.x;
	}

	int expVal;
	if (validLeft) {
		expVal = exploration.at<uchar>(left);
		if (expVal == 1) {
			exploration.at<uchar>(left) = 2;
			costMap.at<int>(left) = curDist + 1;
			queue.push(left);
		}
	}
	if (validRight) {
		expVal = exploration.at<uchar>(right);
		if (expVal == 1) {
			exploration.at<uchar>(right) = 2;
			costMap.at<int>(right) = curDist + 1;
			queue.push(right);
		}
	}
	if (validUp) {
		expVal = exploration.at<uchar>(up);
		if (expVal == 1) {
			exploration.at<uchar>(up) = 2;
			costMap.at<int>(up) = curDist + 1;
			queue.push(up);
		}
	}
	if (validDown) {
		expVal = exploration.at<uchar>(down);
		if (expVal == 1) {
			exploration.at<uchar>(down) = 2;
			costMap.at<int>(down) = curDist + 1;
			queue.push(down);
		}
	}
}
// gets the minimum cost neightbor of a point given a costmap
cv::Point MazeSegmenter::getMinCostNeighbor(const cv::Mat &costMap, cv::Point &pt) {
	int maxCol = costMap.cols, maxRow = costMap.rows;
	int minVal = -1, hold;
	cv::Point minPoint(-1, -1);
	cv::Point left(pt.x - 1, pt.y);
	cv::Point right(pt.x + 1, pt.y);
	cv::Point up(pt.x, pt.y - 1);
	cv::Point down(pt.x, pt.y + 1);

	bool validLeft = (0 < left.x) && (left.x < maxCol - 1) && (0 < left.y) && (left.y < maxRow - 1) && (0 < costMap.at<int>(left));
	bool validRight = (0 < right.x) && (right.x < maxCol - 1) && (0 < right.y) && (right.y < maxRow - 1) && (0 < costMap.at<int>(right));
	bool validUp = (0 < up.x) && (up.x < maxCol - 1) && (0 < up.y) && (up.y < maxRow - 1) && (0 < costMap.at<int>(up));
	bool validDown = (0 < down.x) && (down.x < maxCol - 1) && (0 < down.y) && (down.y < maxRow - 1) && (0 < costMap.at<int>(down));

	if (validLeft) {
		minPoint = left;
		minVal = costMap.at<int>(left);
	}

	if (validRight) {
		if (right.x == 154 || right.y == 367) {
			right.x = right.x;
		}
		hold = costMap.at<int>(right);
		if (minVal < 0 || hold < minVal) {// || ((hold == minVal) && ((rand() % 2) == 0))) {
			minPoint = right;
			minVal = hold;
		}
	}
	if (validUp) {
		hold = costMap.at<int>(up);
		if (minVal < 0 || hold < minVal) {// || ( (hold == minVal) && ( (rand() % 2) == 0 ) )) {
			minPoint = up;
			minVal = hold;
		}
	}
	if (validDown) {
		hold = costMap.at<int>(down);
		if (minVal < 0 || hold < minVal) {// || ((hold == minVal) && ((rand() % 2) == 0))) {
			minPoint = down;
			minVal = hold;
		}
	}
	if (minVal < 0) {
		minPoint = cv::Point(-1, -1);
	}
	return minPoint;
}
// Traverses by descending the cost map
bool MazeSegmenter::traverseCostMap(cv::Mat &costMap, const cv::Point &end, std::vector<cv::Point> &path) {
	int maxCol = costMap.cols, maxRow = costMap.rows;
	// add things to the start (ie push back)
	cv::Point u = end;
	path.insert(path.begin(), u);
	while (costMap.at<int>(u) != 1) {
		int uCost = costMap.at<int>(u);
		u = getMinCostNeighbor(costMap, u);
		if ((u.x < 0) || (u.x > maxCol) || (u.y < 0) || (u.y > maxRow)) {
			return false;
		}
		path.insert(path.begin(), u);
	}
	return true;
}
// Generates a series of connected points (ALL) between a start and end point.
// Uses a cost map and descends from the end to the start to make the path.
void MazeSegmenter::bfs_path(const cv::Mat & src, const cv::Point &start, cv::Mat &output) {

	cv::Mat bw, exp;
	src.copyTo(bw);
	src.copyTo(exp);
	cv::Mat costMap = cv::Mat::zeros(src.rows, src.cols, CV_32SC1);

	// 0 is wall, 1 is open path, 2 is visited
	cv::threshold(exp, exp, 1, 1, cv::THRESH_BINARY);

	/*
	1 Breadth-First-Search(Graph, root):
	2
	3     for each node n in Graph:
	4         n.distance = INFINITY
	5         n.parent = NIL
	6
	7     create empty queue Q
	8
	9     root.distance = 0
	10     Q.enqueue(root)
	11
	12     while Q is not empty:
	13
	14         current = Q.dequeue()
	15
	16         for each node n that is adjacent to current:
	17             if n.distance == INFINITY:
	18                 n.distance = current.distance + 1
	19                 n.parent = current
	20                 Q.enqueue(n)
	*/
	std::queue<cv::Point> q;
	cv::Point u;
	exp.at<uchar>(start) = 2;
	costMap.at<int>(start) = 1;
	q.push(start);
	while (!q.empty()) {
		u = q.front();
		q.pop();
		enqueueMarkAdj(u, q, exp, costMap);
	}
	output = costMap;
	return;
}
#pragma endregion Search

#pragma region PATH_SIMPLIFIER
// Simplifies points into a line by only adding the next segment if its distance
// to the current point is over a threshold.
void MazeSegmenter::simplifyPoints2Line(const std::vector<cv::Point> &src, std::vector<cv::Point> &lines, int szThreshold) {
	if (src.size() < 1) { return; }

	std::vector<cv::Point> ret;
	ret.push_back(src[0]);
	cv::Point prev, cur;
	bool dirX, dirY;
	int dx, dy;
	int retPos = 0;
	bool pushed = false;
	for (int i = 1; i < src.size(); ++i) {
		prev = ret[ret.size() - 1];
		cur = src[i];
		dx = std::abs(prev.x - cur.x);
		dy = std::abs(prev.y - cur.y);
		dirX = dx > 1;
		dirY = dy > 1;
		if ((dx > szThreshold) && (dy > szThreshold)) {
			ret.push_back(src[i]);
			pushed = true;
		} else {
			pushed = false;
		}
	}
	if (!pushed) {
		ret.push_back(cur);
	}
	lines = ret;
}

// Simplifies points into a line by taking the direction of segments into account.
// Forces them to be vertical or horizontal.
void MazeSegmenter::simplifyPointsByDirection(const std::vector<cv::Point> &src, std::vector<cv::Point> &output) {
	if (src.size() < 2) { return; }

	const double degreeConversion = 180.0 / CV_PI;
	std::vector<cv::Point> pts = src;
	double angle;
	cv::Point cur, next;
	for (int i = 0; i < src.size() - 1; ++i) {
		cur = pts[i];
		next = pts[i + 1];
		angle = std::atan2(cur.y - next.y, cur.x - next.x) * degreeConversion;
		if (angle < 0) { angle += 360.0; }

		if ((45 <= angle && angle < 135) || (225 <= angle && angle < 315)) {
			pts[i + 1].x = pts[i].x;
		} else {
			pts[i + 1].y = pts[i].y;
		}
	}
	output = pts;
	return;
}
#pragma endregion Path Simplifier

// Reorders FOUR(!!!) points in a clockwise fashion. Not hard limited
void MazeSegmenter::reorderQuadPoints(std::vector<cv::Point> &pt_array) {
	std::vector<cv::Point> temp(pt_array);
	// top left has smallest sum
	const int sMinPos = 0;
	// bottom right has largest sum
	const int sMaxPos = 2;
	// top right has smallest difference y-x
	const int dMinPos = 1;
	// bottom left has largest difference y-x
	const int dMaxPos = 3;

	int s0 = temp[0].x + temp[0].y, s1 = temp[1].x + temp[1].y;
	int s2 = temp[2].x + temp[2].y, s3 = temp[3].x + temp[3].y;
	int d0 = temp[0].y - temp[0].x, d1 = temp[1].y - temp[1].x;
	int d2 = temp[2].y - temp[2].x, d3 = temp[3].y - temp[3].x;
	int sMin = s0, sMax = s0, dMin = d0, dMax = d0;
	pt_array[sMinPos] = temp[0];
	pt_array[dMinPos] = temp[0];
	pt_array[sMaxPos] = temp[0];
	pt_array[dMaxPos] = temp[0];
	// get the min sum -> top left corner. pos = 0
	if (s1 < sMin) {
		sMin = s1;
		pt_array[sMinPos] = temp[1];
	}
	if (s2 < sMin) {
		sMin = s2;
		pt_array[sMinPos] = temp[2];
	}
	if (s3 < sMin) {
		pt_array[sMinPos] = temp[3];
	}
	// get the max sum -> bottom right corner. pos = 2
	if (s1 > sMax) {
		sMax = s1;
		pt_array[sMaxPos] = temp[1];
	}
	if (s2 > sMax) {
		sMax = s2;
		pt_array[sMaxPos] = temp[2];
	}
	if (s3 > sMax) {
		pt_array[sMaxPos] = temp[3];
	}
	// get the smallest diff -> rop right. pos = 3
	if (d1 < dMin) {
		dMin = d1;
		pt_array[dMinPos] = temp[1];
	}
	if (d2 < dMin) {
		dMin = d2;
		pt_array[dMinPos] = temp[2];
	}
	if (d3 < dMin) {
		pt_array[dMinPos] = temp[3];
	}
	// get the largest diff -> rop right. pos = 1
	if (d1 > dMax) {
		dMax = d1;
		pt_array[dMaxPos] = temp[1];
	}
	if (d2 > dMax) {
		dMax = d2;
		pt_array[dMaxPos] = temp[2];
	}
	if (d3 > dMax) {
		pt_array[dMaxPos] = temp[3];
	}
}
