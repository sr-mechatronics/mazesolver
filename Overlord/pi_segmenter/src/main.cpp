#include <iostream>

#include "opencv2/opencv.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#include "MazeSegmenter.h"
#include "PiCommunicator.h"


// B_HEADLESS 0 (=false) for non headless use (ie show windows), 1 otherwise
#define B_HEADLESS 0
// B_VERBOSE 1(=true) for verbose output (ie display informative text), 0 otherwise
#define B_VERBOSE 1

void log(const std::string &str) {
#if B_VERBOSE
	std::cout << str << std::endl;
#endif
}
void display(const std::string &winName, const cv::Mat &image) {
#if !B_HEADLESS
	if (!(image.empty() || winName.empty())) {
		cv::imshow(winName, image);
	}
#endif
}

#pragma region TRACKBAR_HANDLERS
#if !B_HEADLESS
void tbar_handler_wall_contourEpsilon(int pos, void *val) { *((int*)val) = pos; }
// wall HSL
void tbar_handler_wall_h(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_wall_h_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_wall_s(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_wall_s_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_wall_v(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_wall_v_err(int pos, void *val) { *((int*)val) = pos; }
// Present HSL
void tbar_handler_pres_h(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_pres_h_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_pres_s(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_pres_s_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_pres_v(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_pres_v_err(int pos, void *val) { *((int*)val) = pos; }
// Tree HSL
void tbar_handler_tree_h(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_tree_h_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_tree_s(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_tree_s_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_tree_v(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_tree_v_err(int pos, void *val) { *((int*)val) = pos; }
// Agent HSL
void tbar_handler_agent_h(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_agent_h_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_agent_s(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_agent_s_err(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_agent_v(int pos, void *val) { *((int*)val) = pos; }
void tbar_handler_agent_v_err(int pos, void *val) { *((int*)val) = pos; }
#endif
#pragma endregion TRACKBAR_HANDLERS

void initFrameStream(cv::Mat &img, cv::VideoCapture &cap, bool use_webcam, bool use_static) {
	if (use_static && !use_webcam) {
		img = cv::imread("../test03.jpg", CV_LOAD_IMAGE_COLOR);
		if (!img.data) {
			std::cout << "Image not loaded. Exiting." << std::endl;
			exit(0);
		}
		return;
	} else if (!use_static && !use_webcam) {
		cap.open("./../test_video02.mp4");
		if (cap.isOpened() == false) {
			std::cout << "No video file found. Exiting. Make sure the path to test_video01.mp4 is correct." << std::endl;
			exit(0);
		}
		return;
	} else {
		if (cap.isOpened()) { cap.release(); }
		
		// use this to see what cameras exist
		for (int i = -2; i < 10; ++i) {
		cap.open(i);
		if (cap.isOpened()) {
		std::cout << "camera: " << i << " is opened" << std::endl;
		cap.release();
		}
		}
		
		cap.open(0);
		if (cap.isOpened() == false) {
			std::cout << "No camera found. Exiting." << std::endl;
			exit(0);
		}
	}
}

void getNextFrame(cv::Mat &img, cv::VideoCapture &cap, bool use_static) {
	if (use_static) {
		img = cv::imread("../test03.jpg", CV_LOAD_IMAGE_COLOR);
		if (!img.data) {
			std::cout << "Image not loaded. Exiting." << std::endl;
			exit(0);
		}
		return;
	}
	cap >> img;
}

struct CameraCalibration {	
	bool b_set = false;

	cv::Mat map1, map2;
	cv::Mat intrinsics, distortion;

	CameraCalibration(const std::string &fn) {
		cv::FileStorage fs(fn, cv::FileStorage::READ);
		
		if (!fs.isOpened()) {
			std::cout << "Camera calibration file could not be opened." << std::endl;
			return;
		}

		cv::Size imageSize;
		
		try {
			fs["image_Width"] >> imageSize.width;
			fs["image_Height"] >> imageSize.height;
			fs["Camera_Matrix"] >> intrinsics;
			fs["Distortion_Coefficients"] >> distortion;
			fs.release();


			//cv::initUndistortRectifyMap(intrinsics, distortion, cv::Mat(), cv::getOptimalNewCameraMatrix(intrinsics, distortion, imageSize, 1, imageSize, 0), imageSize, CV_16SC2, map1, map1);
			std::cout << "Camera calibration file successfully parsed." << std::endl;
			b_set = true;
			fs.release();
			return;
		} catch (std::exception &e) {

			b_set = false;
			std::cout << "Camera calibration file could not be parsed:\n\t" << e.what() << std::endl;
			return;
		}
	}

	void remap(cv::Mat &distorted, cv::Mat &undistorted) {
		if (!b_set) { return; }
		//cv::remap(distorted, undistorted, map1, map2, cv::INTER_NEAREST);
		cv::undistort(distorted, undistorted, intrinsics, distortion);
	}
};

int main(int ac, char** av) {
	// #include <ctime> if using this
	// srand((unsigned int)time(NULL));

	// setup webcam stuff
	bool use_webcam = true, use_static = false;
	cv::VideoCapture cap;

	// setup/allocate image matrices
	cv::Mat img_frame, img_frame_rect, img, img_wall_seg, img_rect, img_rect_bgr, img_walls, img_present, img_tree, img_agent, img_map, img_paths;

	// set up calibration
	const std::string calibration_fn = "./../../camera_calibration.xml";
	CameraCalibration calibration(calibration_fn);

	MazeSegmenter segmenter;
	PiCommunicator communicator;

	bool b_presentPath_found = false, b_treePath_found = false;


	// 
	int val_wall_contourEpsilon = 65;
	// Wall HSV
	// 18, 255
	// 0, 79
	// 206, 202
	int val_wall_h = 30, val_wall_h_err = 39;// 12, val_wall_h_err = 15
	int val_wall_s = 21, val_wall_s_err = 39;//37, val_wall_s_err = 68
	int val_wall_v = 109, val_wall_v_err = 31;//182, val_wall_v_err = 155
	// Present HSV
	int val_pres_h = 142, val_pres_h_err = 17;// 105, val_pres_h_err = 113
	int val_pres_s = 48, val_pres_s_err = 13;// 138, val_pres_s_err = 217
	int val_pres_v = 136, val_pres_v_err = 253;// 136, 253
	// Tree HSV
	int val_tree_h = 51, val_tree_h_err = 7;// 58, 22
	int val_tree_s = 133, val_tree_s_err = 119;// 133, 235
	int val_tree_v = 136, val_tree_v_err = 122;// 136, 253
	// Agent HSV
	/*
	int val_agent_h = 34, val_agent_h_err = 53; //24,30
	int val_agent_s = 132, val_agent_s_err = 38;//97,30
	int val_agent_v = 239, val_agent_v_err = 45;//223,107
	*/
	int val_agent_h = 14, val_agent_h_err = 7;
	int val_agent_s = 144, val_agent_s_err = 61;
	int val_agent_v = 198, val_agent_v_err = 109;


#pragma region VERBOSE_SETUP
	const std::string winFrame = "Frame";
	const std::string winImg = "Image";
	const std::string winWallSeg = "wall seg";
	const std::string winWalls = "Walls";
	const std::string winRect = "Rectified";
	const std::string winPresent = "Present";
	const std::string winTree = "Tree";
	const std::string winAgent = "Agent";
	const std::string winMap = "Map";
	const std::string winPaths = "Paths";
#if !B_HEADLESS
	cv::namedWindow(winFrame, CV_WINDOW_NORMAL);
	cv::namedWindow(winImg, CV_WINDOW_NORMAL);
	cv::namedWindow(winWallSeg, CV_WINDOW_NORMAL);
	cv::namedWindow(winWalls, CV_WINDOW_NORMAL);
	cv::namedWindow(winRect, CV_WINDOW_NORMAL);
	cv::namedWindow(winPresent, CV_WINDOW_NORMAL);
	cv::namedWindow(winTree, CV_WINDOW_NORMAL);
	cv::namedWindow(winAgent, CV_WINDOW_NORMAL);
	cv::namedWindow(winMap, CV_WINDOW_NORMAL);
	cv::namedWindow(winPaths, CV_WINDOW_NORMAL);

	int winSize = 150;
	cv::resizeWindow(winFrame, 2 * winSize, 3 * winSize);
	cv::resizeWindow(winImg, 2 * winSize, 3 * winSize);
	cv::resizeWindow(winWallSeg, 2 * winSize, 5 * winSize);
	cv::resizeWindow(winWalls, 2 * winSize, 3 * winSize);
	cv::resizeWindow(winRect, 2 * winSize, 3 * winSize);
	cv::resizeWindow(winPresent, 2 * winSize, 5 * winSize);
	cv::resizeWindow(winTree, 2 * winSize, 5 * winSize);
	cv::resizeWindow(winAgent, 2 * winSize, 5 * winSize);
	cv::resizeWindow(winMap, 2 * winSize, 3 * winSize);
	cv::resizeWindow(winPaths, 2 * winSize, 3 * winSize);

	int tbar_max = 255;
	int tbar_maxMask = 100;
	int tbar_maxEllipse = 20;
	int tbar_defEllipse = 5;


	cv::createTrackbar("Contour Epsilon (pixels)", winImg, &val_wall_contourEpsilon, 150 * 8, tbar_handler_wall_contourEpsilon, &val_wall_contourEpsilon);

	// Wall HSV
	cv::createTrackbar("H", winWallSeg, &val_wall_h, tbar_max, tbar_handler_wall_h, &val_wall_h);
	cv::createTrackbar("H err", winWallSeg, &val_wall_h_err, tbar_max, tbar_handler_wall_h_err, &val_wall_h_err);
	cv::createTrackbar("S", winWallSeg, &val_wall_s, tbar_max, tbar_handler_wall_s, &val_wall_s);
	cv::createTrackbar("S err", winWallSeg, &val_wall_s_err, tbar_max, tbar_handler_wall_s_err, &val_wall_s_err);
	cv::createTrackbar("V", winWallSeg, &val_wall_v, tbar_max, tbar_handler_wall_v, &val_wall_v);
	cv::createTrackbar("V err", winWallSeg, &val_wall_v_err, tbar_max, tbar_handler_wall_v_err, &val_wall_v_err);

	// Present HSV
	cv::createTrackbar("H", winPresent, &val_pres_h, tbar_max, tbar_handler_pres_h, &val_pres_h);
	cv::createTrackbar("H err", winPresent, &val_pres_h_err, tbar_max, tbar_handler_pres_h_err, &val_pres_h_err);
	cv::createTrackbar("S", winPresent, &val_pres_s, tbar_max, tbar_handler_pres_s, &val_pres_s);
	cv::createTrackbar("S err", winPresent, &val_pres_s_err, tbar_max, tbar_handler_pres_s_err, &val_pres_s_err);
	cv::createTrackbar("V", winPresent, &val_pres_v, tbar_max, tbar_handler_pres_v, &val_pres_v);
	cv::createTrackbar("V err", winPresent, &val_pres_v_err, tbar_max, tbar_handler_pres_v_err, &val_pres_v_err);

	// Tree HSV
	cv::createTrackbar("H", winTree, &val_tree_h, tbar_max, tbar_handler_tree_h, &val_tree_h);
	cv::createTrackbar("H err", winTree, &val_tree_h_err, tbar_max, tbar_handler_tree_h_err, &val_tree_h_err);
	cv::createTrackbar("S", winTree, &val_tree_s, tbar_max, tbar_handler_tree_s, &val_tree_s);
	cv::createTrackbar("S err", winTree, &val_tree_s_err, tbar_max, tbar_handler_tree_s_err, &val_tree_s_err);
	cv::createTrackbar("V", winTree, &val_tree_v, tbar_max, tbar_handler_tree_v, &val_tree_v);
	cv::createTrackbar("V err", winTree, &val_tree_v_err, tbar_max, tbar_handler_tree_v_err, &val_tree_v_err);

	// Agent HSV
	cv::createTrackbar("H", winAgent, &val_agent_h, tbar_max, tbar_handler_agent_h, &val_agent_h);
	cv::createTrackbar("H err", winAgent, &val_agent_h_err, tbar_max, tbar_handler_agent_h_err, &val_agent_h_err);
	cv::createTrackbar("S", winAgent, &val_agent_s, tbar_max, tbar_handler_agent_s, &val_agent_s);
	cv::createTrackbar("S err", winAgent, &val_agent_s_err, tbar_max, tbar_handler_agent_s_err, &val_agent_s_err);
	cv::createTrackbar("V", winAgent, &val_agent_v, tbar_max, tbar_handler_agent_v, &val_agent_v);
	cv::createTrackbar("V err", winAgent, &val_agent_v_err, tbar_max, tbar_handler_agent_v_err, &val_agent_v_err);
#endif
#pragma endregion End Verbose Output setup


	// init the picture stream
	initFrameStream(img_frame, cap, use_webcam, use_static);

	// keep the video capture's whitebalance set
	//double wb_u = cap.get(CV_CAP_PROP_WHITE_BALANCE_U);
	//double wb_v = cap.get(CV_CAP_PROP_WHITE_BALANCE_V);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

	int frame_counter = 0;
	//int fps = cap.get(CV_CAP_PROP_FPS);
	int frame_drop_ratio = 3;   // 30fps, drop 15
	
	while (1) {
		//communicator.sendTest();
		cv::waitKey(10);

		// frame dropper
		if ((!use_webcam && !use_static) && (frame_counter++) % frame_drop_ratio != 0) {
			// grab the next frame, but do not load it.
			cap.grab();
			continue;
		} else {
			frame_counter = frame_counter % frame_drop_ratio;
		}

		// get the next frame
		getNextFrame(img_frame, cap, use_static);

		// if the frame is empty, reinit the stream
		if (img_frame.empty()) {
			log("No frame data. Reinitializing.");
			initFrameStream(img_frame, cap, use_webcam, use_static);
			cv::waitKey(500);
			continue;
		}

		// if using webcam and the calibration is not set, set it
		if (calibration.b_set) {
			//cap.set(CV_CAP_PROP_WHITE_BALANCE_U, wb_u);
			//cap.set(CV_CAP_PROP_WHITE_BALANCE_V, wb_v);

			// display the original then rectify!
			display(winFrame, img_frame);
			calibration.remap(img_frame, img_frame_rect);
			img_frame_rect.copyTo(img_frame);
		}
		display(winImg, img_frame);
		// convert to HSV
		cv::cvtColor(img_frame, img, CV_BGR2HSV);
		

		// segment the walls on hsv values
		segmenter.segment(img, img_wall_seg, val_wall_h, val_wall_h_err, val_wall_s, val_wall_s_err, val_wall_v, val_wall_v_err);
		display(winWallSeg, img_wall_seg);

		// if the image is not rectifiable, continue to the next loop
		if (!segmenter.rectify(img, img_wall_seg, img_rect, val_wall_contourEpsilon)) {
			log("No rectangle extracted. Image could not be rectified. Getting next frame.");
			continue;
		}

#if !B_HEADLESS
		cv::cvtColor(img_rect, img_rect_bgr, CV_HSV2BGR);
		display(winRect, img_rect_bgr);
#endif

		// segment from rectified on hsv:
		// wall
		segmenter.segment(img_rect, img_walls, val_wall_h, val_wall_h_err, val_wall_s, val_wall_s_err, val_wall_v, val_wall_v_err);
		display(winWalls, img_walls);
		
		// present
		segmenter.segment(img_rect, img_present, val_pres_h, val_pres_h_err, val_pres_s, val_pres_s_err, val_pres_v, val_pres_v_err);
		display(winPresent, img_present);
		
		// tree
		segmenter.segment(img_rect, img_tree, val_tree_h, val_tree_h_err, val_tree_s, val_tree_s_err, val_tree_v, val_tree_v_err);
		display(winTree, img_tree);
		// agent
		segmenter.segment(img_rect, img_agent, val_agent_h, val_agent_h_err, val_agent_s, val_agent_s_err, val_agent_v, val_agent_v_err);
		display(winAgent, img_agent);


		// invert the wall color
		cv::bitwise_not(img_walls, img_walls);

		// make quasi skeleton of map
		segmenter.makeSkeleton(img_walls, img_map);
		display(winMap, img_map);


		//move this to...
		// ensure that the blobs are on the map!
		cv::bitwise_or(img_map, img_agent, img_map);
		cv::bitwise_or(img_map, img_present, img_map);
		cv::bitwise_or(img_map, img_tree, img_map);
		display(winMap, img_map);

		// mask the present, tree, and agent blobs with the skeleton
		// (ensures that the center of the resultant blobs are on the map)
		img_present.copyTo(img_present, img_map);
		img_tree.copyTo(img_tree, img_map);
		img_agent.copyTo(img_agent, img_map);



		// extract center of blobs
		cv::Point pt_present, pt_agent, pt_tree;
		if (!segmenter.extractBlobs(img_present, pt_present)) {
			log("Present Blob not found. Getting next frame.");
			continue;
		}
#if !B_HEADLESS
		cv::circle(img_present, pt_present, 3, cv::Scalar(255, 255, 255, 0), 5);
		display(winPresent, img_present);
		cv::circle(img_rect_bgr, pt_present, 5, cv::Scalar(0, 0, 255, 0), 3);
		display(winRect, img_rect_bgr);
#endif

		if (!segmenter.extractBlobs(img_tree, pt_tree)) {
			log("Tree Blob not found. Getting next frame.");
			continue;
		}
#if !B_HEADLESS
		cv::circle(img_tree, pt_tree, 3, cv::Scalar(255, 255, 255, 0), 5);
		display(winTree, img_tree);
		cv::circle(img_rect_bgr, pt_tree, 5, cv::Scalar(0, 0, 255, 0), 3);
		display(winRect, img_rect_bgr);
#endif

		if (!segmenter.extractBlobs(img_agent, pt_agent)) {
			log("Agent Blob not found. Getting next frame.");
			continue;
		}
#if !B_HEADLESS
		cv::circle(img_agent, pt_agent, 3, cv::Scalar(255, 255, 255, 0), 5);
		display(winAgent, img_agent);
		cv::circle(img_rect_bgr, pt_agent, 5, cv::Scalar(0, 0, 255, 0), 3);
		display(winRect, img_rect_bgr);

		// copy to img_paths for display
		img_rect_bgr.copyTo(img_paths);
#endif

		// ... here


		std::vector<cv::Point> pointPresentPath, pointTreePath;
		cv::Mat costMap_to_present, costMap_to_tree;

		segmenter.getCostMap(img_map, pt_agent, costMap_to_present);
		segmenter.getCostMap(img_map, pt_present, costMap_to_tree);

		/*
		std::vector<cv::Point> allEndPoints;
		std::vector<std::vector<cv::Point>> allEndPointPaths;
		segmenter.getAllBranches(costMap_to_present, pt_agent, allEndPoints);
		for (cv::Point end : allEndPoints) {
			std::vector<cv::Point> tempPaths;
			if (segmenter.getPath(costMap_to_present, end, tempPaths)) {
				cv::circle(img_rect_bgr, end, 3, cv::Scalar(255, 0, 0, 0), 4);
				allEndPointPaths.push_back(tempPaths);
			}
		}
		display(winRect, img_rect_bgr);
		//segmenter.simplifyAllPaths(allEndPointPaths);
		*/


		if (!segmenter.getPath(costMap_to_present, pt_present, pointPresentPath)) {
			log("Present Path not found.");
		}
#if !B_HEADLESS
		for (int i = 0; i < pointPresentPath.size() - 1; ++i) {
			cv::line(img_paths, pointPresentPath[i], pointPresentPath[i+1], cv::Scalar(255, 0, 0, 0), 3);
		}
		display(winPaths, img_paths);
#endif
		if (!segmenter.getPath(costMap_to_tree, pt_tree, pointTreePath)) {
			log("Tree Path not found.");
		}
#if !B_HEADLESS
		for (int i = 0; i < pointTreePath.size() - 1; ++i) {
			cv::line(img_paths, pointTreePath[i], pointTreePath[i + 1], cv::Scalar(0, 255, 0, 0), 3);
		}
		display(winPaths, img_paths);
#endif

		const int MAX_NUM_SEGS = 3;
		communicator.parseSendCommand(pointPresentPath, pt_agent, pt_present, std::min(img_paths.rows, img_paths.cols) / MAX_NUM_SEGS);
		


		// release
		img_frame.release();
		img.release();
		img_walls.release();
		img_rect.release();
		img_present.release();
		img_tree.release();
		img_agent.release();
		img_map.release();
		img_paths.release();

		// exit
		int key = cvWaitKey(10);
		if (key == 27) break;
	}
	cv::destroyAllWindows();
}