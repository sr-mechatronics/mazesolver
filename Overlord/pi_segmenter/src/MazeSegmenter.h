#ifndef MAZESEGMENTER_H
#define MAZESEGMENTER_H


#include "opencv2/opencv.hpp"
#include <queue>

class MazeSegmenter {
public:
	MazeSegmenter();

	// Segments and creates a mask based on passed values.
	void segment(const cv::Mat &src, cv::Mat &dst, const double &h, const double &h_err, const double &s, const double &s_err, const double &v, const double &v_err);

	// Tries to find the largest rectangle and then rectifies the image using Homographies, and extracts the ROI
	bool rectify(const cv::Mat &src, const cv::Mat &corners, cv::Mat &dst, const int &epsilon);

	// Attempt at a fast skeleton. For use with the maze itself.
	void makeSkeleton(const cv::Mat &src, cv::Mat &dst);

	// gets the center of the largest blob of an image. Segment/Mask on colors first.
	bool extractBlobs(const cv::Mat &src, cv::Point &result);

	// extracts a path between two points given a skeleton.
	bool getPath(cv::Mat &costMap, const cv::Point &end, std::vector<cv::Point> &pathPoints);
	// extracts a path between two points given a skeleton.
	void getCostMap(const cv::Mat &src, const cv::Point &start, cv::Mat &costMap);

	// Gets the branches and paths from start to end of all paths
	void getAllBranches(const cv::Mat &costMap, const cv::Point &start, std::vector<cv::Point> &points);

private:
	const int threshold_points2Lines = 20;

	// Reorders FOUR(!!!) points in a clockwise fashion. Not hard limited
	void reorderQuadPoints(std::vector<cv::Point> &pt_array);

	// SEARCHING
	// enqueues and marks adjacent valid nodes (points)
	void enqueueMarkAdj(cv::Point &pt, std::queue<cv::Point> &queue, cv::Mat &exploration, cv::Mat &costMap);
	// gets the minimum cost neightbor of a point given a costmap
	cv::Point getMinCostNeighbor(const cv::Mat &costMap, cv::Point &pt);
	// Traverses by descending the cost map
	bool traverseCostMap(cv::Mat &costMap, const cv::Point &end, std::vector<cv::Point> &path);
	// Generates a series of connected points (ALL) between a start and end point.
	// Uses a cost map and descends from the end to the start to make the path.
	void bfs_path(const cv::Mat & src, const cv::Point &start, cv::Mat &output);

	// GET ALL PATHS
	// Gets the local maxima of a region. Mat is single channle mat of ints, and 0 is ignored.
	bool getLocalMaxima(const cv::Mat &src, const int &y, const int &y_end, const int &x, const int &x_end, cv::Point &maxima);

	// PATH SIMPLIFICATION
	// Simplifies points into a line by only adding the next segment if its distance
	// to the current point is over a threshold.
	void simplifyPoints2Line(const std::vector<cv::Point> &src, std::vector<cv::Point> &lines, int szThreshold);
	// Simplifies points into a line by taking the direction of segments into account.
	// Forces them to be vertical or horizontal.
	void simplifyPointsByDirection(const std::vector<cv::Point> &src, std::vector<cv::Point> &output);


};
#endif