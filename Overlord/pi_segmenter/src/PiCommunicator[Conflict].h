#ifndef PICOMMUNICATOR_H
#define PICOMMUNICATOR_H


#include <vector>
#include "opencv2/opencv.hpp"

#include <stdio.h>
#include <unistd.h>			//Used for UART
#include <fcntl.h>			//Used for UART
#include <termios.h>		//Used for UART

/*
establish connection()
push path to arduino
recieve commands from arduino
push commands
*/
class PiCommunicator {
public:
	PiCommunicator();
	
	void receiveCommands();
	
	void parseSendCommand(const std::vector<cv::Point> &path, const cv::Point &agent, const cv::Point &target, const int &min_seg);
	void sendTest() {
		if (uart0_filestream == -1) {
			init();
			if (uart0_filestream == -1) {
				return;
			}
		}

		//----- TX BYTES -----
		unsigned char tx_buffer[20];
		unsigned char *p_tx_buffer;

		p_tx_buffer = &tx_buffer[0];
		*p_tx_buffer++ = 'M';

		if (uart0_filestream != -1) {
			int count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));		//Filestream, bytes to write, number of bytes to write
			if (count < 0) {
				printf("UART TX error\n");
			}
		}
	}
private:
	// use enum class Name {}; to avoid unscoped collisions
	enum class Direction {
		LEFT = 0,
		RIGHT = 1,
		UP = 2,
		DOWN = 3
	};
	enum class Command {
		INVALID = -1,
		LEFT = 0,
		RIGHT = 1,
		STRAIGHT = 2,
		ABOUTFACE = 3
	};

	const double _RAD_TO_DEG = 180.0 / CV_PI;


	int uart0_filestream;

	cv::Point prevPoint;

	int init();
	int sendCommand(const Command &cmd);

	Command makeCommand(const cv::Point &agent, const cv::Point &pt1, const cv::Point &pt2, const int &min_seg);
	Direction parseAngle(const double &angle);
	Command parseDirections(const Direction &dir1, const Direction &dir2);
	
};
#endif