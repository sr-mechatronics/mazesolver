#include "PiCommunicator.h"

PiCommunicator::PiCommunicator() {
	//-------------------------
	//----- SETUP USART 0 -----
	//-------------------------
	//At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively
	uart0_filestream = -1;

	init();
}
int PiCommunicator::init() {


	//OPEN THE UART
	//The flags (defined in fcntl.h):
	//	Access modes (use 1 of these):
	//		O_RDONLY - Open for reading only.
	//		O_RDWR - Open for reading and writing.
	//		O_WRONLY - Open for writing only.
	//
	//	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
	//											if there is no input immediately available (instead of blocking). Likewise, write requests can also return
	//											immediately with a failure status if the output can't be written immediately.
	//
	//	O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become the controlling terminal for the process.
	uart0_filestream = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);		//Open in non blocking read/write mode
	if (uart0_filestream == -1) {
		//ERROR - CAN'T OPEN SERIAL PORT
		printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
	}

	//CONFIGURE THE UART
	//The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
	//	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
	//	CSIZE:- CS5, CS6, CS7, CS8
	//	CLOCAL - Ignore modem status lines
	//	CREAD - Enable receiver
	//	IGNPAR = Ignore characters with parity errors
	//	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
	//	PARENB - Parity enable
	//	PARODD - Odd parity (else even)
	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;		//<Set baud rate
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);


	this->prevPoint = cv::Point(-1, -1);
}

PiCommunicator::Command PiCommunicator::makeCommand(const cv::Point &agent, const cv::Point &pt1, const cv::Point &pt2, const int &min_seg) {
	// determine if getting closer to next target
	// if getting farther away, turn around
	// else check if within the minimum distance to take a turn
	//     if within distance, take turn
	//     else continue straight
	double dist_prevNext = pow(prevPoint.x - pt1.x, 2) + pow(prevPoint.y - pt1.y, 2);
	double dist_agentNext = pow(agent.x - pt1.x, 2) + pow(agent.y - pt1.y, 2);
	/*
	if (dist_agentNext > min_seg) {
		return Command::STRAIGHT;
	} else {
		*/
		double a1 = std::atan2(agent.y - pt1.y, agent.x - pt1.x) * _RAD_TO_DEG;
		double a2 = std::atan2(pt1.y - pt2.y, pt1.x - pt2.x) * _RAD_TO_DEG;

		Direction dir1 = parseAngle(a1), dir2 = parseAngle(a2);
		return parseDirections(dir1, dir2);
	//}
	/*
	if ((prevPoint.x < 0 || prevPoint.y < 0) || (dist_prevNext < dist_agentNext)) {
		dist_agentNext = sqrt(dist_agentNext);
		if (dist_agentNext > min_seg) {
			return Command::STRAIGHT;
		} else {
			const double DEG_CONV = 0;
			double a1 = std::atan2(agent.y - pt1.y, agent.x - pt1.x) * _RAD_TO_DEG;
			double a2 = std::atan2(pt1.y - pt2.y, pt1.x - pt2.x) * _RAD_TO_DEG;

			Direction dir1 = parseAngle(a1), dir2 = parseAngle(a2);
			return parseDirections(dir1, dir2);
		}
	} else {
		// send turn around
		return Command::ABOUTFACE;
	}
	*/
}

//  45-135       = 1 = up
// 135-225       = 2 = left
// 225-315       = 3 = down
// 315-360, 0-45 = 0 = right
PiCommunicator::Direction PiCommunicator::parseAngle(const double &angle) {
	double hold = angle;
	if (hold < 0) { hold += 360.0; }

	// up down flipped for coordinate system
	if (45 <= angle && angle < 135) {
		return Direction::DOWN;
	} else if (135 <= angle && angle < 225) {
		return Direction::LEFT;
	} else if (225 <= angle && angle < 315) {
		return Direction::UP;
	} else {
		return Direction::RIGHT;
	}
}

PiCommunicator::Command PiCommunicator::parseDirections(const Direction &dir1, const Direction &dir2) {
	switch (dir1) {
		case Direction::UP:
			std::cout << "dir1==UP\t";
			break;
		case Direction::DOWN:
			std::cout << "dir1==DOWN\t";
			break;
		case Direction::LEFT:
			std::cout << "dir1==LEFT\t";
			break;
		case Direction::RIGHT:
			std::cout << "dir1==RIGHT\t";
			break;
	}
	switch (dir2) {
		case Direction::UP:
			std::cout << "dir2==UP\t";
			break;
		case Direction::DOWN:
			std::cout << "dir2==DOWN\t";
			break;
		case Direction::LEFT:
			std::cout << "dir2==LEFT\t";
			break;
		case Direction::RIGHT:
			std::cout << "dir2==RIGHT\t";
			break;
	}
	if (dir1 == dir2) {
		return Command::STRAIGHT;
	}
	switch (dir1) {
		case Direction::RIGHT:
			if (dir2 == Direction::UP) {
				std::cout << "Moving: LEFT" << std::endl;
				return Command::LEFT;
			} else if (dir2 == Direction::DOWN) {
				std::cout << "Moving: RIGHT" << std::endl;
				return Command::RIGHT;
			}
			break;
		case Direction::UP:
			if (dir2 == Direction::RIGHT) {
				std::cout << "Moving: RIGHT" << std::endl;
				return Command::RIGHT;
			} else if (dir2 == Direction::LEFT) {
				std::cout << "Moving: LEFT" << std::endl;
				return Command::LEFT;
			}
			break;
		case Direction::LEFT:
			if (dir2 == Direction::UP) {
				std::cout << "Moving: RIGHT" << std::endl;
				return Command::RIGHT;
			} else if (dir2 == Direction::DOWN) {
				std::cout << "Moving: LEFT" << std::endl;
				return Command::LEFT;
			}
			break;
		case Direction::DOWN:
			if (dir2 == Direction::LEFT) {
				std::cout << "Moving: RIGHT" << std::endl;
				return Command::RIGHT;
			} else if (dir2 == Direction::RIGHT) {
				std::cout << "Moving: LEFT" << std::endl;
				return Command::LEFT;
			}
			break;
	}
	return Command::INVALID;
}


int PiCommunicator::sendCommand(const Command &cmd) {
	if (uart0_filestream == -1) {
		init();
		if (uart0_filestream == -1) {
			return -1;
		}
	}
	//----- TX BYTES -----
	unsigned char tx_buffer[20];
	unsigned char *p_tx_buffer;

	p_tx_buffer = &tx_buffer[0];


	// send here
	/*
	INVALID = -1,
	LEFT = 0,
	RIGHT = 1,
	STRAIGHT = 2,
	ABOUTFACE = 3
	*/
	switch (cmd) {
		case Command::LEFT:
			*p_tx_buffer++ = 'l';
			break;
		case Command::RIGHT:
			*p_tx_buffer++ = 'r';
			break;
		case Command::STRAIGHT:
			*p_tx_buffer++ = 's';
			break;
		case Command::ABOUTFACE:
			*p_tx_buffer++ = 'a';
			break;
		default:
			*p_tx_buffer++ = 'i';
			break;
	}
	std::cout << "Moving:\t" << &tx_buffer[0] << std::endl;

	if (uart0_filestream != -1) {
		int count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));		//Filestream, bytes to write, number of bytes to write
		if (count < 0) {
			printf("UART TX error\n");
			return -1;
		}
	}


	return 1;
}

void PiCommunicator::parseSendCommand(const std::vector<cv::Point> &path, const cv::Point &agent, const cv::Point &target, const int &min_seg) {
	size_t pathSize = path.size();
	// path size of 2 is agent->target, so continue straight
	// path size larger than 2 is agent->...->turn->...->target
	Command cmd;
	if (pathSize < 2) {
		// send straight
		cmd = Command::STRAIGHT;
	} else {
		// pathsize > 2 ie >=3 therefore it has 
		cmd = makeCommand(agent, path[1], path[2], min_seg); // indices are at least 0, 1, 2
	}

	prevPoint = cv::Point(agent);

	sendCommand(cmd);
}
