# define new project
PROJECT(Mechatronics_Pi_Segmenter)
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.0 FATAL_ERROR)

if(UNIX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif(UNIX)


# Set shared lib build for the rest
SET(BUILD_SHARED_LIBS ON)

# Find dependencies
set(OpenCV_DIR "D:/Libraries/OpenCV/2.4.12/build/")
FIND_PACKAGE(OpenCV REQUIRED)


# Set header and source files
SET(pi_segmenter_HEADERS
    src/TrackbarHandler.h
    src/MazeSegmenter.h
    src/PiCommunicator.h
)

SET(pi_segmenter_SOURCES
    src/main.cpp
    src/MazeSegmenter.cpp
    src/PiCommunicator.cpp
)

# define executable
ADD_EXECUTABLE(${PROJECT_NAME} ${pi_segmenter_HEADERS} ${pi_segmenter_SOURCES})

# define additional include directories and linking targets
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS} ${XBEE_INCLUDE_DIR})
TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${OpenCV_LIBS} ${OPENGL_glu_LIBRARY})