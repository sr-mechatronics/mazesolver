%% Mechatronics final
clear, clc, close all
%P = 120*6894.76; % pressure
%m = 0.25*0.453592; % kg mass
%D = 0.308*0.0254; % m diameter traveling in
%L = 6*0.0254; % m length traveling in

%A = 0.25*pi*D^2;
%F = P*A;
%a = F/m;
%t = sqrt(2*L/a)
%v0 = a*t
%v = sqrt(v0^2 -2*9.81*(2.4384))

% Force to penetrate
F = 6*0.453592*9.81 % lbs to kg then force in N

dist_ceil = 8 * .3048;  % Feet to meters
mass = 0.1 * 0.453592;  % Lbs to kilgorams

P1 = 180 * 6894.76;     % PSI to Pa
L1 = (1.5) * 0.0254;     % in to m
R1 = (0.5)/2 *0.0254;   % in to m
V1 = L1*pi*R1^2+ (4.43+0.78+2+6/16)*pi*(0.25/2)^2

L2 = (4.43+0.78+3)*0.0254; % Barrel
R2 = 0.25/2 * 0.0254%R1;
V2 = L2*pi*R2^2 + V1;

% assume PV [1] = PV [2]
% ie volume is the same at the start of the barrel
% as at the end of the tank
P2 = P1*V1/V2
Pavg = (V1+V2)*(P1*V1)/(2*V1*V2)

F = Pavg * pi*R2^2; % force = pressure * area = mass * accel
accel = F / mass;
time = sqrt(2*L2/accel);%d = 0.5 * a * t^2
v0 = accel*time;
vf = sqrt(v0^2 - 2 * 9.81 * dist_ceil);

fprintf('Final System \n');
fprintf('v0 = %f m/s\n', v0);
fprintf('vf = %f m/s\n', vf);
