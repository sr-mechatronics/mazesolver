#include <Servo.h>
Servo servo;

const int MotorPin = 3; // Assign pin number for motor
const int ButtonPin = 10;
const int SensorPin = A2; //brown (purple is power, blue is ground
int MotorAngle = 90;
int moving = 1;
float value = 0;
boolean done = false;
boolean foundmax = false;
boolean firsttime = true;
float maxvalue = 0;
int i = 0;
unsigned long start;

void setup() {
  servo.attach(MotorPin); // Attach the motor pin to the servo
  servo.write(0); // Start motor at angle of 0 degrees
  pinMode(ButtonPin, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {
  while (!done) {
    if (digitalRead(ButtonPin) == LOW) {
      //Serial.println(moving);
      moving = -1 * moving;
      delay(200);
      start = micros();
    }
    if (moving < 0) { // Move only for negative numbers
      while (!foundmax && (micros()-start > 3000000)) {
        value = analogRead(SensorPin);
        maxvalue = maxvalue + value;
        i++;
        delay(10);
        if ( i > 300 ) {
          maxvalue = maxvalue / i;
          foundmax = true;
          Serial.println(maxvalue);
        }
      }
      
      value = analogRead(SensorPin);
      Serial.println(value);
      if ((value > (maxvalue+2)) && (micros() - start > 6000000)) {  //526
        servo.writeMicroseconds(1500);
        delay(50);
        done = true;
      }
      else {
        servo.write(0);
        delay(50);
      }
    }
    else {
      servo.writeMicroseconds(1500);
      delay(50);
    }
  }
  servo.writeMicroseconds(1500);
  delay(50);
}
