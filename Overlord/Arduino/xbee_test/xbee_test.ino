/*
 Breakout -> Arduino
 5V       -> 5V
 GND      -> GND
 DIN      -> Tx
 DOUT     -> Rx

 SO:
    DOUT(=TX on xbee) is connected to Arduino SoftSerial RX
    DIN (=RX on xbee) is connected to Arduino SoftSerial TX

 On the Mega, only the following can be used for RX (support hardware interrupts):
    10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
 */
//char char_buf[4];
int x;
long NUM = 4;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {}
  Serial1.begin(9600);
  while (!Serial1) {}
}

void loop() {
  // sends data to the xBee SoftwareSerial device
  if (Serial1.available()) {
    Serial.println((char)Serial1.read());
  }
  delay(100); // ms
}
