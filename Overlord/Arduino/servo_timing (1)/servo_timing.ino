#include <Servo.h>
Servo servo;

const int MotorPin = 7; // Assign pin number for motor
const int ButtonPin = 3;
const int ReversePin = 10;
int MotorAngle = 90;
int moving = 1;
int move_direction = 1000;
unsigned long start;

void setup() {
  servo.attach(MotorPin); // Attach the motor pin to the servo
  //servo.write(0); // Start motor at angle of 0 degrees
  pinMode(ButtonPin, INPUT_PULLUP);
  pinMode(ReversePin, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(ButtonPin) == LOW) { // Forward
    moving = -1 * moving;
    move_direction = 1000;
    delay(200);
    start = millis();
  }
  if (digitalRead(ReversePin) == LOW) {
    moving = -1 * moving;
    move_direction = 2000;    
    delay(200);
    start = millis();
  }
  if ((moving < 0) && (abs(millis() - start) < 17000)) { // Move only for negative numbers
    servo.writeMicroseconds(move_direction);
    delay(50);
  }
  else {
    servo.writeMicroseconds(1500);
    delay(50);
  }
}

