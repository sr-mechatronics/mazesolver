#include <SPI.h>
#include <Pixy.h>
#include <Servo.h>

#include "Path.h"
#include "IR_Reader.h"



enum SearchStage {
  AWAITING_COMMANDS,
  FINDING_PRESENT,
  FINDING_TREE,
  DONE
};
enum SearchStrat {
  FOLLOW_COMMANDS,
  FOLLOW_TRAVERSAL
};

/*PINS*/
// XBee's SoftSerial Pins on the Mega. Uno can be any pins
//const int soft_rx = 8, soft_tx = 9;
// WHEEL PINS
const int left_wheel = 4, right_wheel = 5;
// IR PINS
const int pin_leftIR = A0, pin_rightIR = A1, pin_frontIR = A2;
// GRIPPER PINS
const int servoUp_pin = 6, servoClose_pin = 9;
// GRIPPER SWITCH
const int gripperSwitch = 8;


/*OBJECTS*/
Servo left, right;          // continuous servos
Servo servoUp, servoClose;  // normal servos
Pixy pixy;

IR_Reader leftIR(pin_leftIR, IR_Reader::IR_TYPE::NEW), rightIR(pin_rightIR, IR_Reader::IR_TYPE::NEW), frontIR(pin_frontIR, IR_Reader::IR_TYPE::OLD);
Path pathToPresent, pathToTree;


/*
   3 IR's   - 1 Sig Pin each
   1 Pixy   - 4 Pins for SPI - MOSI, MISO, SCK, SS
   4 Servos - 1 Sig Pin each - Interrupt based PWM.
            - Fucks with software serial connections. Anything else that uses interrupts
            - Min response time for servos is ~20ms. They use the last recieved signal
              - Look into Software Servo
              - must call SoftwareServo::refresh() every 50ms (wont update more than 20ms)
              - NO INTERRUPTS!!!!
              - NO QUIRKS to arbitrary values
            -

*/


/*STRUCT AND ENUM VARIABLES*/
// SEARCH STAGE
SearchStage curStage = AWAITING_COMMANDS;
// SEARCH STRATEGY
SearchStrat curStrat = FOLLOW_COMMANDS;


/*COUNTERS*/
// NUMERIC
int numPathQueries = 0;
// TIME
long start_time_xbee = -1;
unsigned long lastTurn = 0;
// WALL DISTANCES (LEFT, RIGHT, FRONT)
double dist_curLeft, dist_curRight, dist_curFront;

const int numAvg = 50;  // Move to up top
double arr[numAvg];  // Move to up top


/*THRESHOLDS*/
// TIME
long timeout_xbee = 10000;  // 10s in millis
long timeout_pixy = 500;    // 0.5s in millis
// NUMERIC
int thresholdPathQuery = -1;


/*VARIABLES/CONSTANTS*/
// FORWARD SPEED

// left wheel positive is faster
// right wheel negative is faster
// Stefan - Change to 1560 and 1440 - <3 Stefan
const int left_wheel_speed = 1600, right_wheel_speed = 1410;//1550, 1450
// BACKUP SPEED
const int left_backup_speed = 1400, right_backup_speed = 1590;
// STOPPING SPEED
const int stop_speed = 1500;  // Stefan - Update this so the wheels dont freak when both set to stop - <3 Stefan
// LEFT/RIGHT SPEED
const int turn_speed_left = 1300, turn_speed_right = 1700;
// SPEED INCREMENT FOR LEFT/RIGHT and FWD/REV
const int speed_increment = 67;
// DISTANCES FROM THE WALL (WALL BUFFERS) (cm)
const double wallBufferMax = 10.0, wallBufferMin = 4.5, turnBuffer = 17.0;
const double turnAroundBuffer = 13.2; //12.8
const int lastTurnBuffer = 1500;
// PIXY SIGNATURES
const int red = 1, green = 2;
// PIXY CENTER AND ERROR
const int pixyCenter = 160, centeringError = 5;
// DISTANCES FROM THE TARGETS (cm)
const double giftDistance = 4.0, treeDistance = 4.0;
// GRIPPER POSITIONS
const int servoUp_upPos = 5, servoUp_downPos = 75, servoUp_upLimit = 5, servoUp_downLimit = 75;
const int servoClose_openPos = 24, servoClose_cloesLimit = 90;
const int servoClose_closePos = 79; // Stefan - Fiddle with this with full battery (+ and -) - <3 Stefan
const int PinchDiamond = 70;  // Not Used


/*
   If lots of turns happen within a short period, do turns by different angles
*/

void setup() {
  // setup serial and wait for it to initialize. disable this
  Serial.begin(9600);
  while (!Serial) {}
  Serial.println(F("Serial monitor initialized. Hello user! - Love Charbot"));

  pixy.init();

  left.attach(left_wheel);
  right.attach(right_wheel);

  servoUp.attach(servoUp_pin);
  servoUp.write(servoUp_upPos);

  servoClose.attach(servoClose_pin);
  servoClose.write(servoClose_openPos);

  // for demo2
  /*
    delay(5000);
    servoUp.write(servoUp_downPos);
    delay(330);
    servoClose.write(servoClose_closePos);
    delay(500);
    servoClose.detach();
    servoUp.write(servoUp_upPos);
    delay(1000);
  */
  // for demo2

  //pinMode(gripperSwitch, INPUT_PULLUP);

  Serial.println(F("Setup complete. Entering Main State Machine."));
  Serial.println(F("=================================================="));

}

void loop() {
  /*
    Serial.print("Left:\t"); Serial.println(leftIR.getReading());
    Serial.print("Right:\t"); Serial.println(rightIR.getReading());
    Serial.print("Front:\t"); Serial.println(frontIR.getReading());
    delay(30);
    return;
  */

  // read data from xbee
  // parse data from xbee
  // get currentSearchStage
  // get currentSearchStrat
  Serial.print(F("Main Loop():\tNext Function Call is "));
  switch (curStage) {
    case DONE:
      Serial.println(F("actionsDone()"));
      actionsDone();
      break;
    case FINDING_PRESENT:
      Serial.println(F("findPresent()"));
      findPresent();
      break;
    case FINDING_TREE:
      Serial.println(F("findTree()"));
      findTree();
      break;
    case AWAITING_COMMANDS:
      Serial.println(F("waitForCommands()"));
      waitForCommands();
      break;
    default:
      break;
  }
  Serial.println(F("=================================================="));
}

// do nothing
void actionsDone() {
  // this causes the servos to spazz out.
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  return;
}


void waitForCommands() {
  curStrat = FOLLOW_TRAVERSAL;
  curStage = FINDING_PRESENT;
  return;

  if (Serial.available()) {
    // if data is corrupted or incorrectly recieved, try again
    // if data is positively invalid, continue with traversal algorithm
    // if data is good with good path, continue with commands


    // Get Data

    // while front of xbee's buffer is not ':' (note ':' has been removed from buffer after read)
    while (Serial.available() > 0 && (Serial.read() != ':')) {}

    String dataXBee = Serial.readStringUntil(':');

    // split dataXBee on delimeter -> ','
    // parse each segment and add to the respective segment

    // possibly more efficient implementation would be to make a new Path struct and use that if the
    // new one is valid, in case an invaliud one gets sent

    Path *curPath;
    String temp;
    for (int i = 0, prev = 0; i > 0; i = dataXBee.indexOf(',', prev)) {
      temp = dataXBee.substring(prev, i);
      if (prev == 0) {
        if (temp.compareTo("present_path")) {
          curPath = &pathToPresent;
          curPath->reset();
        } else if (temp.compareTo("present_path")) {
          curPath = &pathToTree;
        } else if (temp.compareTo("no_path")) {
          ++numPathQueries;
          // increase the number of failed queries
        }
        if (curPath->isValid()) {
          return;
        }
      }
      curPath->addSegment(atoi(temp.c_str()));
      prev = i + 1;
    }
    // all data is good with all paths
    if (pathToPresent.isValid() && pathToTree.isValid()) {
      curStrat = FOLLOW_COMMANDS;
      curStage = FINDING_PRESENT;
      return;
    }
  }
  // if waiting thresholds are exceeded, use the traversal algorithm
  if ((numPathQueries > thresholdPathQuery) || abs(start_time_xbee - millis()) > timeout_xbee) {
    // if timeout then continue with traversal algorithm
    curStrat = FOLLOW_TRAVERSAL;
    curStage = FINDING_PRESENT;
  }
}

bool verifyTarget(const int &target) {
  // Gripper Switch is pullup
  // LOW  = 0 = false = Switch Closed
  // HIGH = 1 = true  = Switch Open
  // bool gripperState = digitalRead(gripperSwitch);
  return true;
  if (target == red) {
    return !digitalRead(gripperSwitch);
  } if (target == green) {
    // switch open = no present in gripper
    return digitalRead(gripperSwitch);
  }
  return false;
}

void findPresent() {

  // if goalInSight move to and act on goal
  // else move
  Serial.print(F("findPresent():\t"));
  if (isTargetInRange(red)) {
    Serial.println(F("Next function is moveToTarget()"));
    // move to present

    // if moveToTarget successful, verify the target
    if (moveToTarget(red, giftDistance)) {
      // if target verified
      if (verifyTarget(red)) {
        Serial.println(F("Present verified in gripper."));
        curStage = FINDING_TREE;
        return;
      } else {
        left.writeMicroseconds(left_backup_speed);
        right.writeMicroseconds(right_backup_speed);
        delay(150);
        left.writeMicroseconds(stop_speed);
        right.writeMicroseconds(stop_speed);
      }
    }

    // else do it again
  } else {
    Serial.println(F("Next function is followStrategy()"));
    followStrategy();
  }
}

void findTree() {
  // should verify that the present is in the gripper before continuing.
  /*
    if (!verifyTarget(red)) {
    curStage = FINDING_PRESENT;
    }
  */

  // if goalInSight move to and act on goal
  // else move
  Serial.print(F("findTree():\t"));
  if (isTargetInRange(green)) {
    Serial.println(F("Next function is moveToTarget()"));
    // move to tree

    if (moveToTarget(green, treeDistance)) {
      // if target verified
      if (verifyTarget(green)) {
        Serial.println(F("Present verified out of gripper."));
        curStage = DONE;
        return;
      } else {
        // correct? try and find the present again
      }
    }
  } else {
    Serial.println(F("Next function is followStrategy()"));
    followStrategy();
  }
}

// checks if the target is in range. If the index < 0 signature not found
bool isTargetInRange(const int &targetSignature) {
  int index = getTargetLargestBlock(targetSignature);
  if (index < 0) {
    Serial.print(F("isTargetInRange(): false:\t"));
    Serial.println(index);
    return false;
  }
  Serial.print(F("isTargetInRange(): true:\t"));
  Serial.println(index);
  return true;
}

// gets the X,Y of the largest block. returns whether the signature was found
bool getTargetXY(const int &targetSignature, int &x, int &y) {
  int index = getTargetLargestBlock(targetSignature);
  if (index < 0) {
    return false;
  }
  x = pixy.blocks[index].x;
  y = pixy.blocks[index].y;
  return true;
}

// returns the index of the largest found block of a given signature
// returns -1 if unsuccessful
int getTargetLargestBlock(const int &targetSignature) {
  const long timeout_pixy_blocks = 250;
  long time_start_pixy_blocks;
  bool b_timeout = false;

  int numBlocks = pixy.getBlocks();

  time_start_pixy_blocks = millis();
  int j = 0;
  while (numBlocks < 1 && !b_timeout) {
    if ((++j % 50) == 0) {
      numBlocks = pixy.getBlocks();
    }
    //j++;
    b_timeout = abs(millis() - time_start_pixy_blocks) < timeout_pixy_blocks;
  }


  Serial.println(F("\n==========START=====PIXY=========="));
  Serial.print(F("Blocks found:\t"));
  Serial.println(numBlocks);

  if (b_timeout || (numBlocks < 1)) {
    return -1;
  }
  int pos_maxBlock = -1;
  double area_maxBlock = -1, area_curBlock;
  for (int i = 0; i < numBlocks; ++i) {
    Serial.print(F("Block:\t"));
    Serial.print(i);
    Serial.print(F("\tSig:\t"));
    Serial.println(pixy.blocks[i].signature);

    area_curBlock = pixy.blocks[i].width * pixy.blocks[i].height;
    if ((pixy.blocks[i].signature == targetSignature) && area_curBlock > area_maxBlock) {
      area_maxBlock = area_curBlock;
      pos_maxBlock = i;
    }
  }
  Serial.println(F("===========END======PIXY==========\n"));
  return pos_maxBlock;
}



bool moveToTarget(const int &targetSignature, const int &targetDist) {
  Serial.println(F("moveToTarget():"));
  int x, y;
  const static long timeout_getTarget = 50;
  const static long timeout_centering = 500; // 0.25 seconds in millis
  unsigned long centering_start = millis(), acquiring_start;
  bool b_acquiring_timedout = false;


  while (abs(centering_start - millis()) < timeout_centering) {
    // while the target is not found in the frame, loop iuntil it is found.
    // not that this is blocking an should instead timeout, then attempt to verify that
    // the target is in fact visible, and if it is not, search for it
    acquiring_start = millis();
    while (!getTargetXY(targetSignature, x, y) && !b_acquiring_timedout) {
      b_acquiring_timedout = abs(acquiring_start - millis()) > timeout_getTarget;
      Serial.println(F("Target not acquired. Looping to acquire"));
    }
    if (b_acquiring_timedout) {
      Serial.println(F("Target acquisition timed out."));
    } else {
      Serial.println(F("Target acquired."));
    }



    // if target in range pick it up
    // else move to x,y
    double dist_front = frontIR.getReading();//getWallDistance(front_IR);
    if (dist_front < 0.0) {
      Serial.println(F("Invalid Front Sensor Reading. Ending moveToTarget()"));
      return false;
    }

    // if distance is less than a value
    if (dist_front <= targetDist) {
      Serial.print(F("Target in range of gripper:\t"));
      left.writeMicroseconds(stop_speed);
      right.writeMicroseconds(stop_speed);
      if (targetSignature == red) {
        Serial.println(F("Grabbing present."));
        servoClose.attach(servoClose_pin);
        delay(500);
        // open servo
        // lower servo
        servoClose.write(servoClose_openPos);
        delay(500);
        servoUp.write(servoUp_downPos);
        delay(500);

        // close servo
        // raise servo
        servoClose.write(servoClose_closePos);
        delay(500);
        servoClose.detach();
        servoUp.write(servoUp_upPos);
        delay(500);


      } else if (targetSignature == green) {
        Serial.println(F("Dropping present."));
        // open servo
        servoClose.attach(servoClose_pin);
        delay(500);
        servoClose.write(servoClose_openPos);
        //curState = VERIFY_TREE_ACQUIRED;
        delay(1000);
        servoClose.detach();
      }
      return true;
    } else if (b_acquiring_timedout) {
      Serial.println(F("moveToTarget() Timeout. Retrying moveToTarget()"));
    } else {
      Serial.print(F("Centering "));
      if (x < (pixyCenter - centeringError)) { // Turn left a little
        Serial.println(F("to the left"));
        left.writeMicroseconds(left_wheel_speed - speed_increment);
        right.writeMicroseconds(right_wheel_speed - speed_increment);
      } else if (x > (pixyCenter + centeringError)) { // Turn right a little
        Serial.println(F("to the left"));
        left.writeMicroseconds(left_wheel_speed + speed_increment);
        right.writeMicroseconds(right_wheel_speed + speed_increment);
      } else {
        Serial.println(F("straight"));
      }
      delay(20);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
    }
  }
  return false;
}


void followStrategy() {
  // save the previous data
  double dist_prevLeft = dist_curLeft;
  double dist_prevRight = dist_curRight;
  double dist_prevFront = dist_curFront;
  // update current data
  dist_curLeft = leftIR.getReading();//kalman_leftIR.update_step(getWallDistance(left_IR));
  dist_curRight = rightIR.getReading();//kalman_rightIR.update_step(getWallDistance(right_IR));
  dist_curFront = frontIR.getReading();//getWallDistance(front_IR);

  Serial.print(F("Left:\t")); Serial.println(dist_curLeft);
  Serial.print(F("Right:\t")); Serial.println(dist_curRight);
  Serial.print(F("Front:\t")); Serial.println(dist_curFront);

  switch (curStrat) {
    case FOLLOW_COMMANDS:
      // if there are no more commands
      //     if the present is visible
      //       move to it
      //     else
      //       ask the pi to update
      // else (there are more commands)
      //     if you are at a junction
      //       make a turn and increment command count
      //     else
      //       move straight
      break;
    case FOLLOW_TRAVERSAL:
      // keep track of turns and timeouts!
      if (dist_curLeft > turnBuffer) {
        Serial.println(F("Turning Left"));
        if ((millis() - lastTurn) > lastTurnBuffer) {
          turnLeft();
          lastTurn = millis();
        }
      } else if (dist_curFront < turnAroundBuffer) {
        if (dist_curRight > turnBuffer) {
          Serial.println(F("Turning Right"));
          if ((millis() - lastTurn) > lastTurnBuffer) {
            turnRight();
            lastTurn = millis();
          }
        } else {
          Serial.println(F("Turning Around"));
          if ((millis() - lastTurn) > lastTurnBuffer) {
            turnAround();
            lastTurn = millis();
          }
        }
      } else {
        Serial.println(F("Moving Straight"));
        moveStraight(dist_prevLeft, dist_prevRight);
      }
      break;
    default:
      break;
  }
  return;
}

// code to move straight
void moveStraight(const double &dist_prevLeft, const double &dist_prevRight) {
  //
  double deltaL = dist_curLeft - dist_prevLeft, deltaR = dist_curRight - dist_prevRight;
  const double threshold_correction = 0.075; // cm // Stefan - Change to 0.1 or 0.8 - <3 Stefan
  const long delay_corrections = 50; // milliseconds
  bool validLeft = isValidMesurement(dist_curLeft) && isValidMesurement(dist_prevLeft);
  bool validRight = isValidMesurement(dist_curRight) && isValidMesurement(dist_prevRight);

  // if dist_curLeft < wallBuffer -> move right
  // if dist_curRight < wallBuffer -> move left
  if (dist_curLeft < wallBufferMin) {
    // correct to right
    left.writeMicroseconds(left_wheel_speed + speed_increment);
    right.writeMicroseconds(right_wheel_speed + speed_increment);
    delay(delay_corrections);
    left.writeMicroseconds(left_wheel_speed);
    right.writeMicroseconds(right_wheel_speed);
  }
  if (dist_curRight < wallBufferMin) {
    // correct to left
    left.writeMicroseconds(left_wheel_speed - speed_increment);
    right.writeMicroseconds(right_wheel_speed - speed_increment);
    delay(delay_corrections);
    left.writeMicroseconds(left_wheel_speed);
    right.writeMicroseconds(right_wheel_speed);
  }

  if (validLeft && validRight) {
    if (-threshold_correction < deltaL && deltaL < threshold_correction
        && -threshold_correction < deltaR && deltaR < threshold_correction) {
      // move straight
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      delay(delay_corrections);
      return;
    } else if (deltaL <= 0 && deltaR >= 0) {
      // correct to right
      left.writeMicroseconds(left_wheel_speed + speed_increment);
      right.writeMicroseconds(right_wheel_speed + speed_increment);
      delay(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    } else if (deltaL >= 0 && deltaR <= 0) {
      // correct to left
      left.writeMicroseconds(left_wheel_speed - speed_increment);
      right.writeMicroseconds(right_wheel_speed - speed_increment);
      delay(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    } else {
      // correct to right and pray
      left.writeMicroseconds(left_wheel_speed + 2 * speed_increment);
      right.writeMicroseconds(right_wheel_speed);
      delay(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    }
  } else if (validLeft) {
    if (-threshold_correction < deltaL && deltaL < threshold_correction) {
      // move straight
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      delayMicroseconds(delay_corrections);
      return;
    } else if (deltaL <= 0) {
      // correct to right
      left.writeMicroseconds(left_wheel_speed + speed_increment);
      right.writeMicroseconds(right_wheel_speed + speed_increment);
      delayMicroseconds(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    } else if (deltaL >= 0) {
      // correct to left
      left.writeMicroseconds(left_wheel_speed - speed_increment);
      right.writeMicroseconds(right_wheel_speed - speed_increment);
      delayMicroseconds(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    }
  } else if (validRight) {
    if (-threshold_correction < deltaR && deltaR < threshold_correction) {
      // move straight
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      delayMicroseconds(delay_corrections);
      return;
    } else if (deltaR >= 0) {
      // correct to right
      left.writeMicroseconds(left_wheel_speed + speed_increment);
      right.writeMicroseconds(right_wheel_speed + speed_increment);
      delayMicroseconds(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    } else if (deltaR <= 0) {
      // correct to left
      left.writeMicroseconds(left_wheel_speed - speed_increment);
      right.writeMicroseconds(right_wheel_speed - speed_increment);
      delayMicroseconds(delay_corrections);
      left.writeMicroseconds(left_wheel_speed);
      right.writeMicroseconds(right_wheel_speed);
      return;
    }
  } else {
    // move straight
    left.writeMicroseconds(left_wheel_speed);
    right.writeMicroseconds(right_wheel_speed);
    delayMicroseconds(delay_corrections);
  }
}

void turnLeft() {
  delay(75); // Was 400 when worked perfectly
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  left.writeMicroseconds(turn_speed_left);
  right.writeMicroseconds(turn_speed_left);
  delay(490);
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  left.writeMicroseconds(left_wheel_speed);
  right.writeMicroseconds(right_wheel_speed);

  double left_dist = leftIR.getReading();//getWallDistance(left_IR);
  long start = millis();
  const int timeout = 2000;
  while (left_dist > turnBuffer) {
    if (millis() - start > timeout) {
      Serial.println(F("Wall Timeout"));
      left.writeMicroseconds(left_backup_speed);
      right.writeMicroseconds(right_backup_speed);
      delay(200);
     // left.writeMicroseconds(left_wheel_speed);
     // right.writeMicroseconds(right_wheel_speed);
      break;
    }
    left_dist = leftIR.getReading();//getWallDistance(left_IR);
  }
  delay(50);
}

void turnRight() {
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  //left.writeMicroseconds(left_backup_speed);
  //right.writeMicroseconds(right_backup_speed);
  //delay(250);
  //left.writeMicroseconds(stop_speed);
  //right.writeMicroseconds(stop_speed);
  //delay(100);
  left.writeMicroseconds(turn_speed_right);
  right.writeMicroseconds(turn_speed_right);
  delay(505);
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  left.writeMicroseconds(left_wheel_speed);
  right.writeMicroseconds(right_wheel_speed);
}

void turnAround() {
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  left.writeMicroseconds(left_backup_speed);
  right.writeMicroseconds(right_backup_speed);
  delay(100);
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(30);
  left.writeMicroseconds(turn_speed_right);
  right.writeMicroseconds(turn_speed_right);
  delay(1000);
  left.writeMicroseconds(stop_speed);
  right.writeMicroseconds(stop_speed);
  delay(100);
  left.writeMicroseconds(left_wheel_speed);
  right.writeMicroseconds(right_wheel_speed);
}

// checks whether the current measurement (cm) is valid.
bool isValidMesurement(const int &measurement) {
  static const double minMeasurement = 3.5, maxMeasurement = 37.5;
  return (minMeasurement <= measurement) && (measurement <= maxMeasurement);
}



