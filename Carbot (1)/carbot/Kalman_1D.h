#ifndef KALMAN_1D_H
#define KALMAN_1D_H

// 1D Kalman Filter for the IR sensors
class Kalman_1D {
  private:
    double q = 0.000005; // process noise covariance
    double r = 0.000005;  // measurement noise covariance
                        // This value came from tuning. Too large and values needed to settle.
                        // Too small and they jump around
    double x = 0;       // current state
    double p = 1;       // estimation error covariance
    double k;           // kalman gain

    void measurementUpdate() {
      this->k = (this->p + this->q) / (this->p + this->q + this->r);
      this->p = this->r * (this->p + this->q) / (this->r + this->p + this->q);
    }
  public:
    Kalman_1D() {}
    Kalman_1D(const int &process_cov, const int &measurement_cov, const int &estimation_cov) {
      this->q = process_cov;
      this->r = measurement_cov;
      this->p = estimation_cov;
    }
    Kalman_1D(const int &process_cov, const int &measurement_cov, const int &estimation_cov,
              const int &initial_value) {
      Kalman_1D(process_cov, measurement_cov, estimation_cov);
      this->x = initial_value;;
    }



    double update_step(double measurement) {
      measurementUpdate();
      double result = this->x + (measurement - this->x) * this->k;
      this->x = result;
      return result;
    }
};

#endif

